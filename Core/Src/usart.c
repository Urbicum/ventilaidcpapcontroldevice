/*
 * usart.c
 *
 *  Created on: 01.04.2020
 *      Author: Maciej Zarnowski
 */

#include "usart.h"

void USART2Init(void)
{
    RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
    RCC->APB1ENR |= RCC_APB1ENR_USART2EN;
    /*USART2 GPIO Configuration
	PA2     ------> USART2_TX (alternate function push-pull), max speed 50MHz
	PA3     ------> USART2_RX (input floating) (reset state)
	*/
    GPIOA->CRL = GPIOA->CRL | GPIO_CRL_MODE2;
    GPIOA->CRL &= ~GPIO_CRL_CNF2; //reset CNF for that port to 00
    GPIOA->CRL = GPIOA->CRL | GPIO_CRL_CNF2_1; //alternate function push-pull

    //REF MAN page 818
    USART2->BRR = 36000000UL / USART2_BAUD_RATE;

    USART2->CR1 = USART_CR1_UE | USART_CR1_TE | USART_CR1_RE | USART_CR1_TCIE;
    USART2->CR3 = USART_CR3_DMAT | USART_CR3_DMAR | USART_CR3_EIE; // Error interrupt enable

    NVIC_EnableIRQ(USART2_IRQn);

    USART2ConfigureRxDma();

    USART2_Tx_In_Progress = 0;
}

void USART2ConfigureTxDma(volatile uint8_t* _buffer, uint16_t _buffer_size)
{
    if (USART2_Tx_In_Progress == 0) {
        //setup DMA transmission - REF MAN page 812 for USART and REF MAN page 278 for DMA
        //USART2_TX is DMA1 Channel 7
        RCC->AHBENR |= RCC_AHBENR_DMA1EN; //enable DMA clock
        DMA1_Channel7->CCR &= ~DMA_CCR_EN; //disable DMA channel just in case
        DMA1_Channel7->CPAR = (uint32_t)&USART2->DR; //write usart_dr register address in the dma control register
        DMA1_Channel7->CMAR = (uint32_t)(_buffer); //write memory address in DMA control register to configure source of the transfer
        DMA1_Channel7->CNDTR = _buffer_size; //configure total number of bytes to be transferred by DMA
        DMA1_Channel7->CCR = DMA_CCR_PL | DMA_CCR_PSIZE_1 | DMA_CCR_MINC | DMA_CCR_DIR | DMA_CCR_TEIE; //configure channel priority in DMA control register (should be highest possible)
        DMA1->ISR &= ~DMA_ISR_TCIF7; //write 0 to TC bit in SR register
        DMA1_Channel7->CCR |= DMA_CCR_EN; //activate channel in DMA control register

        NVIC_EnableIRQ(DMA1_Channel7_IRQn);

        USART2_Tx_In_Progress = 1;
    } else {
        //discard that message, we'll send another soon...
        //TODO: circular buffer
    }
}

void USART2ConfigureRxDma(void)
{
    //setup DMA reception mode - REF MAN page 814 for uart and REF MAN page 278 for DMA
    //USART2_RX is DMA1 Channel 6
    NVIC_DisableIRQ(DMA1_Channel6_IRQn);

    RCC->AHBENR |= RCC_AHBENR_DMA1EN; //enable DMA clock
    DMA1_Channel6->CCR &= ~DMA_CCR_EN; //disable DMA channel
    DMA1_Channel6->CPAR = (uint32_t)&USART2->DR; //write usart_dr register address in the dma control register
    DMA1_Channel6->CMAR = (uint32_t)USART2_Rx_Buffer; //write memory address in DMA control register to configure destination of the transfer
    DMA1_Channel6->CNDTR = USART2_RX_BUFFER_SIZE; //configure total number of bytes to be transferred by DMA
    DMA1_Channel6->CCR = DMA_CCR_PL | DMA_CCR_PSIZE_1 | DMA_CCR_MINC | DMA_CCR_TEIE | DMA_CCR_CIRC; //configure channel priority in DMA control register (should be highest possible), configure interrupt generation
    DMA1_Channel6->CCR |= DMA_CCR_EN; //activate channel in DMA control register

    NVIC_EnableIRQ(DMA1_Channel6_IRQn);
}

uint16_t USART2GetCurrentBufferHead(void)
{
    return (uint16_t)(USART2_RX_BUFFER_SIZE - (DMA1_Channel6->CNDTR));
}

uint8_t USART2RxBufferReadByte(uint8_t* buffer, uint16_t start_element, uint16_t element_number)
{

    uint8_t read_value;

    if ((start_element + element_number) >= USART2_RX_BUFFER_SIZE) {
        //overflow condition, so we have to calculate next element
        read_value = *(buffer + (element_number - (USART2_RX_BUFFER_SIZE - start_element)));
    } else {
        //normal condition
        read_value = *(buffer + (start_element) + element_number);
    }
    return read_value;
}
void USART2TxBufferWriteBytes(volatile uint8_t* output_buffer, uint8_t* input_buffer, uint8_t size)
{
    USART_Tx_Buffer_users++;
    uint16_t USART_Tx_Buffer_temp_index = USART_Tx_Buffer_start_index;
    USART_Tx_Buffer_start_index += size;
    USART_Tx_Buffer_start_index %= USART2_TX_BUFFER_SIZE;

    for (uint8_t ii = 0; ii < size; ii++) {
        USART2TxBufferWriteByte(output_buffer, USART_Tx_Buffer_temp_index, ii, input_buffer[ii]);
    }

    USART_Tx_Buffer_tail += size;
    USART_Tx_Buffer_tail %= USART2_TX_BUFFER_SIZE;

    USART_Tx_Buffer_users--;
}

void USART2TxBufferWriteByte(volatile uint8_t* buffer, uint16_t start_element, uint16_t element_number, uint16_t write_value)
{

    if ((start_element + element_number) >= USART2_TX_BUFFER_SIZE) {
        //overflow condition, so we have to calculate next element
        *(buffer + (element_number - (USART2_TX_BUFFER_SIZE - start_element))) = write_value;

    } else {
        //normal condition
        *(buffer + (start_element) + element_number) = write_value;
    }
}

uint16_t USART2RxBufferGetBytesInBuffer(uint16_t head, uint16_t tail)
{

    uint16_t number_of_bytes;

    if (head > tail) {
        number_of_bytes = (USART2_RX_BUFFER_SIZE + tail - head);
    } else {
        number_of_bytes = tail - head;
    }
    return number_of_bytes;
}

//2DO refactor
uint16_t USART2TxBufferGetBytesInBuffer(uint16_t head, uint16_t tail)
{

    uint16_t number_of_bytes;

    if (head > tail) {
        number_of_bytes = (USART2_TX_BUFFER_SIZE + tail - head);
    } else {
        number_of_bytes = tail - head;
    }
    return number_of_bytes;
}

void USART2PushDataOut(void)
{
    if ((USART_Tx_Buffer_head != USART_Tx_Buffer_tail) && !USART_Tx_Buffer_users) {
        uint16_t nbytes_to_send = USART2TxBufferGetBytesInBuffer(USART_Tx_Buffer_head, USART_Tx_Buffer_tail);
        if (USART_Tx_Buffer_tail > USART_Tx_Buffer_head) {
            USART2ConfigureTxDma(USART_Tx_Buffer + USART_Tx_Buffer_head, nbytes_to_send);
        } else {
            uint16_t nbytes_to_end_of_buf = USART2_TX_BUFFER_SIZE - USART_Tx_Buffer_head;
            USART_Tx_Buffer_bytes_left = nbytes_to_send - nbytes_to_end_of_buf;
            USART2ConfigureTxDma(USART_Tx_Buffer + USART_Tx_Buffer_head, nbytes_to_end_of_buf);
        }
    }
}
