/**
  ******************************************************************************
  * @file    EEPROM_Emulation/inc/eeprom.h 
  * @author  MCD Application Team
  * @version V1.5.0
  * @date    14-April-2017
  * @brief   This file contains all the functions prototypes for the EEPROM 
  *          emulation firmware library.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  * Modified by Maciej Zarnowski
  *
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __EEPROM_H
#define __EEPROM_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Exported constants --------------------------------------------------------*/
/* Base address of the Flash sectors */
#define ADDR_FLASH_PAGE_124 ((uint32_t)0x0803E000) /* Base @ of Page 124, 2 Kbytes */
#define ADDR_FLASH_PAGE_125 ((uint32_t)0x0803E800) /* Base @ of Page 125, 2 Kbytes */
#define ADDR_FLASH_PAGE_126 ((uint32_t)0x0803F000) /* Base @ of Page 126, 2 Kbytes */
#define ADDR_FLASH_PAGE_127 ((uint32_t)0x0803F800) /* Base @ of Page 127, 2 Kbytes */

/* Define the size of the sectors to be used */
#define PAGE_SIZE (uint32_t) FLASH_PAGE_SIZE /* Page size */

/* EEPROM start address in Flash */
#define EEPROM_START_ADDRESS ((uint32_t)ADDR_FLASH_PAGE_124) /* EEPROM emulation start address */

/* Pages 0 and 1 base and end addresses */
#define EEPROM_PAGE0_BASE_ADDRESS ((uint32_t)(EEPROM_START_ADDRESS + 0x0000))
#define EEPROM_PAGE0_END_ADDRESS ((uint32_t)(EEPROM_START_ADDRESS + (PAGE_SIZE - 1)))
#define EEPROM_PAGE0_ID ADDR_FLASH_PAGE_124

#define EEPROM_PAGE1_BASE_ADDRESS ((uint32_t)(EEPROM_START_ADDRESS + 0x800 + 0x800))
#define EEPROM_PAGE1_END_ADDRESS ((uint32_t)(EEPROM_START_ADDRESS + 0x800 + 0x800 + PAGE_SIZE - 1))
#define EEPROM_PAGE1_ID ADDR_FLASH_PAGE_126

/* Used Flash pages for EEPROM emulation */
#define EEPROM_PAGE0 ((uint16_t)0x0000)
#define EEPROM_PAGE1 ((uint16_t)0x0002)

/* No valid page define */
#define EEPROM_NO_VALID_PAGE ((uint16_t)0x00AB)

/* Page status definitions */
#define EEPROM_ERASED ((uint16_t)0xFFFF) /* Page is empty */
#define EEPROM_RECEIVE_DATA ((uint16_t)0xEEEE) /* Page is marked to receive data */
#define EEPROM_VALID_PAGE ((uint16_t)0x0000) /* Page containing valid data */

/* Valid pages in read and write defines */
#define EEPROM_READ_FROM_VALID_PAGE ((uint8_t)0x00)
#define EEPROM_WRITE_IN_VALID_PAGE ((uint8_t)0x01)

/* Page full define */
#define EEPROM_PAGE_FULL ((uint8_t)0x80)

/* Variables' number */
#define EEPROM_NB_OF_VAR ((uint8_t)50)

uint16_t EepromVirtAddVarTab[EEPROM_NB_OF_VAR];

/* Exported types ------------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
uint16_t EepromInit(void);
uint16_t EepromWriteValue(uint16_t VirtAddress, float data);
uint16_t EepromReadValue(uint16_t VirtAddress, float* data);
#endif /* __EEPROM_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
