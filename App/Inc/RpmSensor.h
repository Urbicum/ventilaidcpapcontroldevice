/*
 * RPMSensor.h
 *
 *  Created on: 19.05.2020
 *      Author: Maciej
 */

#ifndef INC_RPMSENSOR_H_
#define INC_RPMSENSOR_H_

#include "main.h"

void RPMSensorInit(void);
void RPMSensorUpdate(void);
void RPMAverage(void);
uint16_t RPMSensorReadEdges(void);
void RPMSensorResetEdgesCounter(void);
float RPMEstimate(uint16_t PWM);
void RPMIsReached(uint16_t PWM);

#endif /* INC_RPMSENSOR_H_ */
