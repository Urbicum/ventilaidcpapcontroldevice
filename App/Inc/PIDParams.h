/*
 * PIDSettings.h
 *
 *  Created on: 20 May 2020
 *      Author: Piotr
 */

#ifndef INC_PIDPARAMS_H_
#define INC_PIDPARAMS_H_

/* PROTOCOL VERSION V.2 */
// values that are loaded from memory on device init
float PID_MOTPID_P;
float PID_MOTPID_I;
float PID_MOTPID_D;
float PID_MOTPID_MAX;
float PID_MOTPID_MIN;
float PID_PRESPID_P;
float PID_PRESPID_I;
float PID_PRESPID_D;
float PID_PRESPID_MAX;
float PID_PRESPID_MIN;
float PID_PEEPPID_P;
float PID_PEEPPID_I;
float PID_PEEPPID_D;
float PID_PEEPPID_MAX;
float PID_PEEPPID_MIN;

// values that are received from front panel and stored in memory
float PID_SET_MOTPID_P;
float PID_SET_MOTPID_I;
float PID_SET_MOTPID_D;
float PID_SET_MOTPID_MAX;
float PID_SET_MOTPID_MIN;
float PID_SET_PRESPID_P;
float PID_SET_PRESPID_I;
float PID_SET_PRESPID_D;
float PID_SET_PRESPID_MAX;
float PID_SET_PRESPID_MIN;
float PID_SET_PEEPPID_P;
float PID_SET_PEEPPID_I;
float PID_SET_PEEPPID_D;
float PID_SET_PEEPPID_MAX;
float PID_SET_PEEPPID_MIN;

void PIDParamsLoadFromEeprom(void);
void PIDParamsStoreReceivedParams(void);
#endif /* INC_PIDPARAMS_H_ */
