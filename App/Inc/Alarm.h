/*
 * Alarm.h
 *
 *  Created on: 21 May 2020
 *      Author: Piotr
 */

/*
 * # -------- #
 * # WORKFLOW #
 * # -------- #
 *
 * To use some specified type of defined alarm, you need to add it's init function e.g. AlarmOxygenInit() to general AlarmInit() function.
 * Next, it is crucial to add it's check function e.g. AlarmOxygenCheck() to general AlarmCheckAlarmsAndUpdateDeviceStatus() function.
 *
 * IMPORTANT: After adding init and check functions, please ensure that variable or variables that needs to be checked is/are periodically
 * refreshed somewhere in the program code. In case of AlarmOxygen it would be oxygen_pressure and oxygen_concentration variables.
 *
 *
 * NEW ALARM CREATION
 * ------------------
 * If you need add new alarm, you have to do following steps:
 * - Create new instance of alarm (tAlarm type)
 * - Create alarm init function
 * - Create alarm check function
 * - Create new enum with details (set detail bits)
 * - Add alarm init function to general AlarmInit()
 * - Add alarm check function to general AlarmCheckAlarmsAndUpdateDeviceStatus()
 */

#ifndef INC_ALARM_H_
#define INC_ALARM_H_

#include "main.h"

#define ALARM_DEVICE_STATUS_BIT_0 ((uint8_t)0)
#define ALARM_DEVICE_STATUS_BIT_1 ((uint8_t)1)
#define ALARM_DEVICE_STATUS_BIT_2 ((uint8_t)2)
#define ALARM_POWER_SUPPLY_BIT ((uint8_t)3)
#define ALARM_BATTERY_SUPPLY_BIT ((uint8_t)4)
#define ALARM_OXYGEN_BIT ((uint8_t)5)
#define ALARM_AIR_PRESSURE_BIT ((uint8_t)6)
#define ALARM_AIR_VOLUME_BIT ((uint8_t)7)
#define ALARM_HOSES_BIT ((uint8_t)8)
#define ALARM_BREATHS_PER_MINUTE_BIT ((uint8_t)9)
#define ALARM_DEVICE_BIT ((uint8_t)10)

#define ALARM_NO_ALARMS_BIT ((uint8_t)0)

#define ALARM_DEVICE_STATUS_MASK ((uint32_t)(1 << ALARM_DEVICE_STATUS_BIT_0) | (1 << ALARM_DEVICE_STATUS_BIT_1) | (1 << ALARM_DEVICE_STATUS_BIT_2))
#define ALARM_POWER_SUPPLY_MASK ((uint32_t)(1 << ALARM_POWER_SUPPLY_BIT))
#define ALARM_BATTERY_SUPPLY_MASK ((uint32_t)(1 << ALARM_BATTERY_SUPPLY_BIT))
#define ALARM_OXYGEN_MASK ((uint32_t)(1 << ALARM_OXYGEN_BIT))
#define ALARM_AIR_PRESSURE_MASK ((uint32_t)(1 << ALARM_AIR_PRESSURE_BIT))
#define ALARM_AIR_VOLUME_MASK ((uint32_t)(1 << ALARM_AIR_VOLUME_BIT))
#define ALARM_HOSES_MASK ((uint32_t)(1 << ALARM_HOSES_BIT))
#define ALARM_BREATHS_PER_MINUTE_MASK ((uint32_t)(1 << ALARM_BREATHS_PER_MINUTE_BIT))
#define ALARM_DEVICE_MASK ((uint32_t)(1 << ALARM_DEVICE_BIT))

/* BEGIN: Defines for checking alarm conditions */
// TODO: Set correct values
#define POWER_SUPPLY_MIN_VOLTAGE ((float)4.9f)
#define POWER_SUPPLY_MAX_VOLTAGE ((float)5.1f)

#define BATTERY_SUPPLY_MIN_VOLTAGE ((float)3.7f)
#define BATTERY_SUPPLY_MAX_VOLTAGE ((float)4.2f)

#define OXYGEN_PRESSURE_MIN_VALUE ((float)700.0f)
#define OXYGEN_PRESSURE_MAX_VALUE ((float)740.0f)
#define OXYGEN_CONCENTRATION_MIN_VALUE ((float)19.0f)
#define OXYGEN_CONCENTRATION_MAX_VALUE ((float)21.0f)

#define AIR_PRESSURE_MIN_VALUE ((float)1.1f)
#define AIR_PRESSURE_MAX_VALUE ((float)1.5f)

#define AIR_VOLUME_MINUTE_TOTAL_MIN_VALUE ((float)45.0f)
#define AIR_VOLUME_MINUTE_TOTAL_MAX_VALUE ((float)55.0f)
#define AIR_VOLUME_BREATH_VOLUME_MIN_VALUE ((float)45.0f)
#define AIR_VOLUME_BREATH_VOLUME_MAX_VALUE ((float)55.0f)

#define BREATHS_PER_MINUTE_MIN_VALUE ((uint8_t)16)
#define BREATHS_PER_MINUTE_MAX_VALUE ((uint8_t)20)
/* END: Defines for checking alarm conditions */

enum device_status {
    STOP = 0,
    INITIALIZING,
    RUNNING,
    ALARM,
    DEVICE_FAILURE
};

enum Power_supply_detail_bit {
    POWER_SUPPLY_NO_ALARMS = ALARM_NO_ALARMS_BIT,
    POWER_SUPPLY_NO_VOLTAGE,
    POWER_SUPPLY_VOLTAGE_TOO_LOW,
    POWER_SUPPLY_VOLTAGE_TOO_HIGH
};

enum Battery_supply_detail_bit {
    BATTERY_SUPPLY_NO_ALARMS = ALARM_NO_ALARMS_BIT,
    BATTERY_SUPPLY_NO_BATTERY_CONNECTED,
    BATTERY_SUPPLY_VOLTAGE_TOO_LOW,
    BATTERY_SUPPLY_VOLTAGE_TOO_HIGH
};

enum Oxygen_detail_bit {
    OXYGEN_NO_ALARMS = ALARM_NO_ALARMS_BIT,
    OXYGEN_PRESSURE_TOO_LOW,
    OXYGEN_PRESSURE_TOO_HIGH,
    OXYGEN_CONCENTRATION_TOO_LOW,
    OXYGEN_CONCENTRATION_TOO_HIGH
};

enum Air_pressure_detail_bit {
    AIR_PRESSURE_NO_ALARMS = ALARM_NO_ALARMS_BIT,
    AIR_PRESSURE_TOO_LOW,
    AIR_PRESSURE_TOO_HIGH
};

enum Air_volume_detail_bit {
    AIR_VOLUME_NO_ALARMS = ALARM_NO_ALARMS_BIT,
    AIR_VOLUME_MINUTE_TOTAL_TOO_LOW,
    AIR_VOLUME_MINUTE_TOTAL_TOO_HIGH,
    AIR_VOLUME_BREATH_VOLUME_TOO_LOW,
    AIR_VOLUME_BREATH_VOLUME_TOO_HIGH
};

enum Hoses_detail_bit {
    HOSES_NO_ALARMS = ALARM_NO_ALARMS_BIT,
    HOSES_LEAK,
    HOSES_DISCONNECTED
};

enum Breaths_per_minute_detail_bit {
    BREATHS_PER_MINUTE_NO_ALARMS = ALARM_NO_ALARMS_BIT,
    BREATHS_PER_MINUTE_NO_BREATHS,
    BREATHS_PER_MINUTE_TOO_LOW,
    BREATHS_PER_MINUTE_TOO_HIGH
};

enum Device_detail_bit {
    DEVICE_NO_ALARMS = ALARM_NO_ALARMS_BIT,
    DEVICE_MOTOR_ALARM,
    DEVICE_PRESSURE_SENSOR_ALARM,
    DEVICE_USER_PANEL_ALARM
};

typedef struct {
    uint8_t bit_num;
    uint8_t detail_bit;
} tAlarm;

uint32_t Alarm_code;
uint32_t Alarm_code_detail;
uint32_t Alarm_detail;

uint32_t Alarm_reset_code;

enum device_status current_status;

tAlarm Alarm_power_supply;
tAlarm Alarm_battery_supply;
tAlarm Alarm_oxygen;
tAlarm Alarm_air_pressure;
tAlarm Alarm_air_volume;
tAlarm Alarm_hoses;
tAlarm Alarm_breaths_per_minute;
tAlarm Alarm_device;

void AlarmInit(void);
void AlarmCheckAlarmsAndUpdateDeviceStatus(void);
void AlarmSetAlarm(tAlarm Alarm, uint8_t detail_bit);
void AlarmGetDetails(tAlarm Alarm);
void AlarmClearAlarm(tAlarm Alarm);
void AlarmPowerSupplyInit(void);
void AlarmPowerSupplyCheck(void);
void AlarmBatterySupplyInit(void);
void AlarmBatterySupplyCheck(void);
void AlarmOxygenInit(void);
void AlarmOxygenCheck(void);
void AlarmAirPressureInit(void);
void AlarmAirPressureCheck(void);
void AlarmAirVolumeInit(void);
void AlarmAirVolumeCheck(void);
void AlarmHosesInit(void);
void AlarmHosesCheck(void);
void AlarmBreathsPerMinuteInit(void);
void AlarmBreathsPerMinuteCheck(void);
void AlarmDeviceInit(void);
void AlarmDeviceCheck(void);

#endif /* INC_ALARM_H_ */
