/*
 * MotorOutput.h
 *
 *  Created on: 01.04.2020
 *      Author: Maciej
 */

#ifndef INC_RCPWMOUTPUT_H_
#define INC_RCPWMOUTPUT_H_

#include "main.h"

#define RCPWM_MAP_MAX 1000
#define RCPWM_MAP_MIN 0

// TIM1, rcpwm frequency - 50Hz
#define RCPWM_OUT_MIN 1000 // minimum signal output (us)
#define RCPWM_OUT_MAX 2000 // maximum signal otuput (us)
#define RCPWM_AT_ZERO 990 // maximum signal otuput (us)

#define RCPWM_SERVO_OUT_MIN 540 // minimum signal output (us)
#define RCPWM_SERVO_OUT_MAX 2300 // maximum signal otuput (us)
#define RCPWM_SERVO_AT_ZERO 540 // maximum signal otuput (us)

#define MOTOR_OUTPUT_CHANNEL TIM_CHANNEL_1
#define SERVO0_OUTPUT_CHANNEL TIM_CHANNEL_2
#define SERVO1_OUTPUT_CHANNEL TIM_CHANNEL_3

float MotorOut;
float MotorRPM;
float MotorCurr;
float MotorTemp;

void RcPwmOutputInit(void);

/*
 * _output level should be 0-1000 range
 */
void RcPwmMotorSetOutput(uint16_t _output_level);

void RcPwmServo0SetOutput(uint16_t _output_level);

void RcPwmServo1SetOutput(uint16_t _output_level);

void RcPwmServo0SetOutputRaw(uint16_t _output_level);

void RcPwmServo1SetOutputRaw(uint16_t _output_level);

#endif /* INC_RCPWMOUTPUT_H_ */
