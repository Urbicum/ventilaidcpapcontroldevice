/*
 * LED.h
 *
 *  Created on: 01.04.2020
 *      Author: Maciej
 *      Uses LED WS2812B
 */

#ifndef INC_LED_H_
#define INC_LED_H_

#include "main.h"

#define LED_LOGIC_1 0b110
#define LED_LOGIC_0 0b100

void LedInit(void);

void LedSetRgb(uint8_t _R, uint8_t _G, uint8_t _B);

#endif /* INC_LED_H_ */
