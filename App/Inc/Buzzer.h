/*
 * Peep.h
 *
 *  Created on: 06.05.2020
 *      Author: Maciej
 */

#ifndef INC_BUZZER_H_
#define INC_BUZZER_H_

#include "main.h"

/*
 * Initialize GPIO port and sets output Off
 */
void BuzzerInit(void);

/*
 * Sets buzzer output
 * Arguments:
 * _output (1 - output on, 0 - output off)
 */
void BuzzerSetOutput(uint8_t _output);

#endif /* INC_BUZZER_H_ */
