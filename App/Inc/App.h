/*
 * App.h
 *
 *  Created on: 01.04.2020
 *      Author: Maciej
 */

#ifndef INC_APP_H_
#define INC_APP_H_

#include "main.h"

#define BUFFER_SIZE 80
#define SIZE_OF_FLOAT ((uint8_t)4)

#define FALSE ((uint8_t)0)
#define TRUE ((uint8_t)1)

#define START_MARK_1 0xF0
#define START_MARK_2 0x0F
#define MESSAGE_ID_INDEX ((uint8_t)2)
#define MESSAGE_LEN_INDEX ((uint8_t)3)
#define MESSAGE_DATA_INDEX ((uint8_t)4)
#define END_MARK 0xAA

#define MESSAGE_FOOTER_LEN ((uint8_t)3)
#define MESSAGE_HEADER_LEN ((uint8_t)4)

#define SIZEOF(a) ((uint8_t)(sizeof a / sizeof a[0]))

#define MAIN_PARAMS_MSG_ID ((uint8_t)1)
#define MAIN_PARAMS_MSG_LEN ((uint8_t)49)

#define MEDICAL_SETTINGS_PARAMS_MSG_ID ((uint8_t)2)
#define MEDICAL_SETTINGS_PARAMS_MSG_LEN ((uint8_t)32)

#define PID_SETTINGS_PARAMS_MSG_ID ((uint8_t)3)
#define PID_SETTINGS_PARAMS_MSG_LEN ((uint8_t)67)

#define FLOW_CALIBRATION_PARAMS_MSG_ID ((uint8_t)4)
#define FLOW_CALIBRATION_PARAMS_MSG_LEN ((uint8_t)27)

#define WORKPOINT_CALIBRATION_PARAMS_MSG_ID ((uint8_t)5)
#define WORKPOINT_CALIBRATION_PARAMS_MSG_LEN ((uint8_t)27)

#define BREATHALYSER_PARAMS_MSG_ID ((uint8_t)6)
#define BREATHALYSER_PARAMS_MSG_LEN ((uint8_t)23)

#define ALARM_STATUS_MSG_ID ((uint8_t)10)
#define ALARM_STATUS_MSG_LEN ((uint8_t)11)

#define ALARM_DETAILS_MSG_ID ((uint8_t)11)
#define ALARM_DETAILS_MSG_LEN ((uint8_t)15)

#define PRESSURE_SENSOR_0_CALIBRAION_MSG_ID ((uint8_t)20)
#define PRESSURE_SENSOR_0_CALIBRAION_MSG_LEN ((uint8_t)23)

#define PRESSURE_SENSOR_1_CALIBRAION_MSG_ID ((uint8_t)21)
#define PRESSURE_SENSOR_1_CALIBRAION_MSG_LEN ((uint8_t)23)

#define PRESSURE_SENSOR_2_CALIBRAION_MSG_ID ((uint8_t)22)
#define PRESSURE_SENSOR_2_CALIBRAION_MSG_LEN ((uint8_t)23)

#define PRESSURE_SENSOR_3_CALIBRAION_MSG_ID ((uint8_t)23)
#define PRESSURE_SENSOR_3_CALIBRAION_MSG_LEN ((uint8_t)23)

#define DEBUG_PARAMS_MSG_ID ((uint8_t)80)
#define DEBUG_PARAMS_MSG_LEN ((uint8_t)55)

#define REQUEST_MESSAGE_MSG_ID ((uint8_t)100)
#define REQUEST_MESSAGE_MESS_NO_INDEX ((uint8_t)4)

#define REQUEST_MEDICAL_SETTINGS_PARAMS_MSG_ID ((uint8_t)102)
#define REQUEST_MEDICAL_SETTINGS_PARAMS_MODE_INDEX ((uint8_t)28)

#define REQUEST_PID_SETTINGS_PARAMS_MSG_ID ((uint8_t)103)

#define REQUEST_FLOW_CALIBRATION_PARAMS_MSG_ID ((uint8_t)104)

#define REQUEST_WORKPOINT_CALIBRATION_PARAMS_MSG_ID ((uint8_t)105)

#define REQUEST_BREATHALYSER_PARAMS_MSG_ID ((uint8_t)106)

#define REQUEST_RESET_ALARM_MSG_ID ((uint8_t)110)
#define REQUEST_RESET_ALARM_DATA_START_INDEX ((uint8_t)4)

#define REQUEST_PRESSURE_SENSOR_0_CALIBRAION_MSG_ID ((uint8_t)120)

#define REQUEST_PRESSURE_SENSOR_1_CALIBRAION_MSG_ID ((uint8_t)121)

#define REQUEST_PRESSURE_SENSOR_2_CALIBRAION_MSG_ID ((uint8_t)122)

#define REQUEST_PRESSURE_SENSOR_3_CALIBRAION_MSG_ID ((uint8_t)123)

uint16_t app_data_send_rate;
uint16_t uart_send_rate;
volatile uint16_t incoming_buffer_tail;
volatile uint16_t incoming_buffer_head;

typedef enum { waiting_for_start_mark = 0,
    waiting_for_rest_of_message = 1,
    processing_message = 2 } USART2_incoming_message_process_step_e;

typedef struct message_e {
    uint8_t message_id;
    uint8_t message_length;
    uint8_t message_start;
} message_e;

USART2_incoming_message_process_step_e incoming_message_process_step;

message_e current_message;

void AppInit(void);

void AppReceiveFloats(uint8_t* _buffer, uint16_t start, const uint8_t msg_len,
    float* floats_to_receive[], const uint8_t sizeof_floats_to_receive);

void AppFillInOutputBufferAndSend(const uint8_t msg_id, const uint8_t msg_len,
    uint32_t* uints32_to_send[], const uint8_t sizeof_uints32_to_send,
    float* floats_to_send[], const uint8_t sizeof_floats_to_send,
    uint8_t* uints8_to_send[], const uint8_t sizeof_uints8_to_send);

/*void AppProcessReceivedByte(uint8_t _byte);*/

void AppProcessReceivedMessage(uint8_t* _buffer, uint16_t start, uint8_t _message_length);

uint8_t output_buffer[BUFFER_SIZE];

void AppCheckForNewIncomingMessage(void);

void AppSendMainParams(void);
void AppSendMedicalSettingsParams(void);
void AppSendPIDSettingsParams(void);
void AppSendFlowCalibrationParams(void);
void AppSendWorkpointCalibrationParams(void);
void AppSendBreathalyserParams(void);
void AppSendAlarmStatusMessage(void);
void AppSendAlarmDetailsMessage(void);
void AppSendPressureSensor_0_CalibrationMessage(void);
void AppSendPressureSensor_1_CalibrationMessage(void);
void AppSendPressureSensor_2_CalibrationMessage(void);
void AppSendPressureSensor_3_CalibrationMessage(void);
void AppSendDebugParams(void);

#endif /* INC_APP_H_ */
