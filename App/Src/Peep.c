/*
 * Peep.c
 *
 *  Created on: 06.05.2020
 *      Author: Maciej
 */

#include "Peep.h"

#include "tim.h"

void PeepInit(void)
{
    HAL_TIM_PWM_Start(&htim2, PEEP_OUTPUT_CHANNEL);
    __HAL_TIM_SET_COMPARE(&htim2, PEEP_OUTPUT_CHANNEL, PEEP_OUT_MIN);
}

void PeepSetOutput(uint16_t _output)
{
    // set peep output
    __HAL_TIM_SET_COMPARE(&htim2, PEEP_OUTPUT_CHANNEL, _output);
}
