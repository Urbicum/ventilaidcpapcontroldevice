/*
 * Utils.c
 *
 *  Created on: 06.04.2020
 *      Author: Maciej
 */

#include "Utils.h"

union float2bytes {
    float f;
    uint8_t b[4];
};

union float2word {
    float f;
    uint16_t w[2];
};

void ConvertFloatToUintArray(float _input, uint8_t* _output)
{
    union float2bytes f2b;
    f2b.f = _input;
    _output[0] = f2b.b[0];
    _output[1] = f2b.b[1];
    _output[2] = f2b.b[2];
    _output[3] = f2b.b[3];
}

float ConvertUintArrayToFloat(uint8_t* _input_array)
{
    union float2bytes f2b;
    f2b.b[0] = *(_input_array);
    f2b.b[1] = *(_input_array + 1);
    f2b.b[2] = *(_input_array + 2);
    f2b.b[3] = *(_input_array + 3);
    return f2b.f;
}

void ConvertFloatToWordArray(float _input, uint16_t* _output)
{
    union float2word f2w;
    f2w.f = _input;
    _output[0] = f2w.w[0];
    _output[1] = f2w.w[1];
}

float ConvertWordArrayToFloat(uint16_t* _input_array)
{
    union float2word f2w;
    f2w.w[0] = *(_input_array);
    f2w.w[1] = *(_input_array + 1);
    return f2w.f;
}

uint16_t checksum(const uint8_t* data, uint16_t len, checksum_type standard)
{
    uint16_t crc = 0x0000;
    switch (standard) {
    case iso_3309:
        crc = 0xffff;
        break;
    case itu_v41:
        crc = 0x6363;
        break;
    }
    uint8_t c;
    const uint8_t* p = (const uint8_t*)(data);
    while (len--) {
        c = *p++;
        crc = ((crc >> 4) & 0x0fff) ^ crc_tbl[((crc ^ c) & 15)];
        c >>= 4;
        crc = ((crc >> 4) & 0x0fff) ^ crc_tbl[((crc ^ c) & 15)];
    }
    switch (standard) {
    case iso_3309:
        crc = ~crc;
        break;
    case itu_v41:
        break;
    }
    return crc & 0xffff;
}

uint32_t CalculateDeltaT(uint32_t start, uint32_t end)
{
    uint32_t result;
    if (end >= start) {
        result = end - start;
    } else {
        result = ((0xFFFFFFFF - start) + end + 1);
    }
    return result;
}
