/*
 * RPMSensor.c
 *
 *  Created on: 19.05.2020
 *      Author: Maciej
 */

#include "RpmSensor.h"
float RPMBuff[256];
const uint16_t RPMMaxSample = 20;
uint16_t RPMBuffIndex = 0;
uint8_t RPMBuffReady = 0;
float RPMTmp;
float RPMSum;
float rpmsensor_current_speed = 0;
float RPMAvg = 0;
float tmp = 0;
float RPMBand = 0.25;
uint8_t RPMReached = 0;

/*
 * Initialize peripherals used by RPM sensor
 * Input pin -> PC6
 * Timer -> TIM8_CH1
 * Mode -> External clock mode, max 65535 impulses, than it overflows
 *
 */
void RPMDeltaSensorInit(void)
{
    RCC->APB2ENR |= RCC_APB2ENR_IOPCEN | RCC_APB2ENR_TIM8EN; // turn on clocks

    // config PC6 as input

    TIM8->CCMR1 = TIM_CCMR1_CC1S_0;
    TIM8->CCER = TIM_CCER_CC1E;

    TIM8->SMCR = TIM_SMCR_SMS_2 | TIM_SMCR_TS_2 | TIM_SMCR_TS_0; // SLAVE RESET MODE, TRGI = TI1FP1
    TIM8->PSC = 7;
    TIM8->ARR = 0xFFFF;

    TIM8->CR1 = TIM_CR1_CEN;
}

void RPMDeltaSensorUpdate(void)
{
    tmp = (float)((TIM8->PSC + 1) * (TIM8->CCR1));
    if (0 == tmp)
        RPMTmp = 0;
    else
        RPMTmp = ((float)720000 / tmp);

    if (RPMBuffReady == 1) {
        RPMSum -= RPMBuff[RPMBuffIndex];
    }
    RPMSum += RPMTmp;
    RPMBuff[RPMBuffIndex] = RPMTmp;

    RPMBuffIndex++;

    if (RPMMaxSample == RPMBuffIndex) {
        RPMBuffReady = 1;
        RPMBuffIndex = 0;
    }
}

void RPMDeltaAverage(void)
{
    if (RPMBuffReady == 1) {
        RPMAvg = (float)RPMSum * 100 * 60 / (7 * (float)RPMMaxSample);
    } else {
        RPMAvg = (float)RPMSum * 100 * 60 / (7 * (float)RPMBuffIndex);
    }
}

void RPMSigmaSensorInit(void)
{
    RCC->APB2ENR |= RCC_APB2ENR_IOPCEN | RCC_APB2ENR_TIM8EN; // turn on clocks

    TIM8->CCER = TIM_CCER_CC1P;
    TIM8->SMCR = TIM_SMCR_SMS | TIM_SMCR_TS_2 | TIM_SMCR_TS_0; //External Clock Mode 1, TRGI = TI1FP1
    TIM8->ARR = 0xFFFF;
    TIM8->CR1 = TIM_CR1_CEN;
}

/*
 * Function returns rising edges detected since last reset
 */
void RPMSigmaSensorUpdate(void)
{
    RPMTmp = (uint16_t)TIM8->CNT;
    TIM8->CNT = 0;

    if (RPMBuffReady == 1) {
        RPMSum -= RPMBuff[RPMBuffIndex];
    }
    RPMSum += RPMTmp;
    RPMBuff[RPMBuffIndex] = RPMTmp;

    RPMBuffIndex++;

    if (RPMMaxSample == RPMBuffIndex) {
        RPMBuffReady = 1;
        RPMBuffIndex = 0;
    }
}

void RPMSigmaAverage(void)
{
    if (RPMBuffReady == 1) {
        RPMAvg = (float)RPMSum * 100 * 60 / (7 * (float)RPMMaxSample);
    } else {
        RPMAvg = (float)RPMSum * 100 * 60 / (7 * (float)RPMBuffIndex);
    }
}

/*
 * Function returns rising edges detected since last reset
 */
uint16_t RPMSigmaSensorReadEdges(void)
{
    return (uint16_t)TIM8->CNT;
}

/*
 * Function resets the counter value to 0
 */
void RPMSigmaSensorResetEdgesCounter(void)
{
    TIM8->CNT = 0;
}

void RPMSensorInit(void)
{
    RPMDeltaSensorInit();
}

void RPMSensorUpdate(void)
{
    RPMDeltaSensorUpdate();
}

void RPMAverage(void)
{
    RPMDeltaAverage();
}

float RPMEstimate(uint16_t PWM)
{
    return PWM * 24 + 526;
}

void RPMIsReached(uint16_t PWM)
{
    float RPMBot = RPMAvg * (1 - RPMBand);
    float RPMTop = RPMAvg * (1 + RPMBand);
    float RPM = RPMEstimate(PWM);
    if ((RPMBot <= RPM) && (RPM <= RPMTop)) {
        RPMReached = 1;
    } else
        RPMReached = 0;
}
//void RPMSensorResetEdgesCounter(void)
//{
//    RPMDeltaSensorResetEdgesCounter();
//}
//uint16_t RPMSensorReadEdges(void)
//{
//    return RPMDeltaSensorReadEdges();
//}
//
