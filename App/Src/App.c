/*
 * App.c
 *
 *  Created on: 01.04.2020
 *      Author: Maciej
 */

#include "App.h"
#include "Alarm.h"
#include "Breathalyzer.h"
#include "Eeprom.h"
#include "EepromMemoryMap.h"
#include "Error.h"
#include "PressureSensor.h"
#include "Utils.h"
#include "Ventilator.h"
#include "adc.h"
#include "math.h"
#include "usart.h"
#include <FlowParams.h>
#include <PIDParams.h>
#include <PidController.h>
#include <RcPwmOutput.h>
#include <WorkpointParams.h>

extern float PressAvg[4];
extern uint32_t PressTmp[4];
extern uint32_t PressSum[4];
extern uint32_t PressAvgInt[4];
extern float PressFAvgRaw;

extern float pid_kp;
extern float pid_ki;
extern float pid_kd;
extern float pid_sampleTimeSeconds;
extern float pid_minOutput;
extern float pid_maxOutput;
extern PIDControl* pid;
extern float RPMAvg;
extern uint16_t RPMTmp;

extern float PeepPid_kp;
extern float PeepPid_ki;
extern float PeepPid_kd;
extern float PeepPid_sampleTimeSeconds;
extern float PeepPid_minOutput;
extern float PeepPid_maxOutput;
extern PIDControl* PeepPid;

extern float PressPAvg;
extern float PressFAvg;
extern float Press0Avg;
extern float Press1Avg;
extern float Press2Avg;
extern float Press3Avg;
extern float PrVPid;
extern uint16_t CalExhCount;
extern uint16_t CalInhCount;
extern uint8_t InhaleValue;
extern uint16_t output;
extern float CalFlowAvg;
extern float Flow;
extern float FlowSumIn;
extern float FlowSumEx;
extern uint8_t BreathBuffIndex;

extern float uchyb;
extern float peep;

void AppInit(void)
{
    app_data_send_rate = 10;
    uart_send_rate = 1000;
    incoming_message_process_step = waiting_for_start_mark;
}

void AppReceiveFloats(uint8_t* _buffer, uint16_t start, const uint8_t msg_len,
    float* floats_to_receive[], const uint8_t sizeof_floats_to_receive)
{
    float received_value;
    uint8_t conv_val_array[SIZE_OF_FLOAT];

    if ((sizeof_floats_to_receive * SIZE_OF_FLOAT) > msg_len - MESSAGE_FOOTER_LEN - MESSAGE_HEADER_LEN) {
        /* ERROR! Received message has less data than expected to receive. */
        /* TODO: Send debug message here. */
        /* printf("ERROR! File: %s line: %d - sizeof_floats_to_receive*SIZE_OF_FLOAT) > msg_len - MESSAGE_FOOTER_LEN - MESSAGE_HEADER_LEN\r\n", __FILE__, __LINE__); */
        return;
    }

    if (floats_to_receive != NULL && sizeof_floats_to_receive != 0) {
        for (uint8_t j = 0, i = MESSAGE_DATA_INDEX; j < sizeof_floats_to_receive; j++, i++) {
            conv_val_array[0] = USART2RxBufferReadByte(_buffer, start, i);
            conv_val_array[1] = USART2RxBufferReadByte(_buffer, start, ++i);
            conv_val_array[2] = USART2RxBufferReadByte(_buffer, start, ++i);
            conv_val_array[3] = USART2RxBufferReadByte(_buffer, start, ++i);

            received_value = ConvertUintArrayToFloat(conv_val_array);
            *floats_to_receive[j] = received_value;
        }
    }
}

void AppFillInOutputBufferAndSend(const uint8_t msg_id, const uint8_t msg_len,
    uint32_t* uints32_to_send[], const uint8_t sizeof_uints32_to_send,
    float* floats_to_send[], const uint8_t sizeof_floats_to_send,
    uint8_t* uints8_to_send[], const uint8_t sizeof_uints8_to_send)
{
    uint8_t payload_filled = 0;
    uint16_t checksum_val;
    uint8_t conv_val_array[SIZE_OF_FLOAT];

    if (msg_len > BUFFER_SIZE) {
        /* ERROR! Output buffer is smaller than data that needs to be sent. */
        /* TODO: Send debug message here. */
        /* printf("ERROR! File: %s line: %d - msg_len > BUFFER_SIZE\r\n", __FILE__, __LINE__); */
        return;
    }

    for (uint8_t i = 0; i < msg_len; i++) {
        if (i == 0) // Message header
        {
            output_buffer[i] = START_MARK_1; // Start mark 1
            output_buffer[++i] = START_MARK_2; // Start mark 2
            output_buffer[++i] = msg_id; // Message ID
            output_buffer[++i] = msg_len; // Message length
        } else if (i == (msg_len - MESSAGE_FOOTER_LEN)) // Message footer
        {
            checksum_val = checksum((output_buffer + MESSAGE_HEADER_LEN), msg_len - MESSAGE_FOOTER_LEN - MESSAGE_HEADER_LEN, iso_3309);

            output_buffer[i] = checksum_val & 0xFF;
            output_buffer[++i] = (checksum_val >> 8) & 0xFF;
            output_buffer[++i] = END_MARK;
        } else // Message payload
        {
            if (payload_filled != 1) {
                if (uints32_to_send != NULL && sizeof_uints32_to_send != 0) {
                    for (uint8_t j = 0; j < sizeof_uints32_to_send; j++, i++) {
                        output_buffer[i] = *uints32_to_send[j] & 0xFF;
                        output_buffer[++i] = (*uints32_to_send[j] >> 8) & 0xFF;
                        output_buffer[++i] = (*uints32_to_send[j] >> 16) & 0xFF;
                        output_buffer[++i] = (*uints32_to_send[j] >> 24) & 0xFF;
                    }
                }

                if (floats_to_send != NULL && sizeof_floats_to_send != 0) {
                    for (uint8_t j = 0; j < sizeof_floats_to_send; j++, i++) {
                        ConvertFloatToUintArray(*floats_to_send[j], conv_val_array);

                        output_buffer[i] = conv_val_array[0];
                        output_buffer[++i] = conv_val_array[1];
                        output_buffer[++i] = conv_val_array[2];
                        output_buffer[++i] = conv_val_array[3];
                    }
                }

                if (uints8_to_send != NULL && sizeof_uints8_to_send != 0) {
                    for (uint8_t j = 0; j < sizeof_uints8_to_send; j++, i++) {
                        output_buffer[i] = *uints8_to_send[j];
                    }
                }

                i--;
                payload_filled = 1;
            } else // Payload filled
            {
                output_buffer[i] = 0; // Fill rest of output the buffer with zeroes
            }
        }
    }

    USART2TxBufferWriteBytes(USART_Tx_Buffer, output_buffer, msg_len); // Send message
}

void AppProcessReceivedMessage(uint8_t* _buffer, uint16_t start, uint8_t _message_length)
{
    uint8_t message_id = USART2RxBufferReadByte(_buffer, start, MESSAGE_ID_INDEX);
    uint8_t message_len = USART2RxBufferReadByte(_buffer, start, MESSAGE_LEN_INDEX);

    /* UNCOMMENT IF NEEDED
     *
    if (message_len != _message_length) // Check if length of the expected message is the same as length in receive buffer
    {
        // ERROR! Message length in receive buffer is not equal expected.
        // TODO: Send debug message here.
        // printf("ERROR! File: %s line: %d - message_len != _message_length\r\n", __FILE__, __LINE__);
    	return;
    }
    */

    switch (message_id) {
    case REQUEST_MESSAGE_MSG_ID: //settings message
    {
        ventilator_mess_no = USART2RxBufferReadByte(_buffer, start, REQUEST_MESSAGE_MESS_NO_INDEX);
        switch (ventilator_mess_no) {
        case MAIN_PARAMS_MSG_ID:
            AppSendMainParams();
            break;
        case MEDICAL_SETTINGS_PARAMS_MSG_ID:
            AppSendMedicalSettingsParams();
            break;
        case PID_SETTINGS_PARAMS_MSG_ID:
            AppSendPIDSettingsParams();
            break;
        case FLOW_CALIBRATION_PARAMS_MSG_ID:
            AppSendFlowCalibrationParams();
            break;
        case WORKPOINT_CALIBRATION_PARAMS_MSG_ID:
            AppSendWorkpointCalibrationParams();
            break;
        case BREATHALYSER_PARAMS_MSG_ID:
            AppSendBreathalyserParams();
            break;
        case ALARM_STATUS_MSG_ID:
            AppSendAlarmStatusMessage();
            break;
        case ALARM_DETAILS_MSG_ID:
            AppSendAlarmDetailsMessage();
            break;
        case PRESSURE_SENSOR_0_CALIBRAION_MSG_ID:
            AppSendPressureSensor_0_CalibrationMessage();
            break;
        case PRESSURE_SENSOR_1_CALIBRAION_MSG_ID:
            AppSendPressureSensor_1_CalibrationMessage();
            break;
        case PRESSURE_SENSOR_2_CALIBRAION_MSG_ID:
            AppSendPressureSensor_2_CalibrationMessage();
            break;
        case PRESSURE_SENSOR_3_CALIBRAION_MSG_ID:
            AppSendPressureSensor_3_CalibrationMessage();
            break;
        case DEBUG_PARAMS_MSG_ID:
            AppSendDebugParams();
            break;
        default:
            break;
        }

        break;
    }
    case REQUEST_MEDICAL_SETTINGS_PARAMS_MSG_ID: {
        float* floats_to_receive[] = {
            &ventilator_medical_set_PEEP,
            &ventilator_medical_set_TargetFiO2,
            &ventilator_medical_set_PIP,
            &ventilator_medical_set_IEratio,
            &ventilator_medical_set_BreathPerMin,
            &ventilator_medical_set_TargetVol
        };
        AppReceiveFloats(_buffer, start, message_len, floats_to_receive, SIZEOF(floats_to_receive));
        VentilatorStoreReceivedMedicalSettings();
        ventilator_medical_Mode = USART2RxBufferReadByte(_buffer, start, REQUEST_MEDICAL_SETTINGS_PARAMS_MODE_INDEX);
        break;
    }
    case REQUEST_PID_SETTINGS_PARAMS_MSG_ID: {
        float* floats_to_receive[] = {
            &PID_SET_MOTPID_P,
            &PID_SET_MOTPID_I,
            &PID_SET_MOTPID_D,
            &PID_SET_MOTPID_MAX,
            &PID_SET_MOTPID_MIN,
            &PID_SET_PRESPID_P,
            &PID_SET_PRESPID_I,
            &PID_SET_PRESPID_D,
            &PID_SET_PRESPID_MAX,
            &PID_SET_PRESPID_MIN,
            &PID_SET_PEEPPID_P,
            &PID_SET_PEEPPID_I,
            &PID_SET_PEEPPID_D,
            &PID_SET_PEEPPID_MAX,
            &PID_SET_PEEPPID_MIN
        };
        AppReceiveFloats(_buffer, start, message_len, floats_to_receive, SIZEOF(floats_to_receive));

        //store values in nonvolatile memory
        EepromWriteValue(EEPROM_ADDR_MOTPID_P, PID_SET_MOTPID_P);
        EepromWriteValue(EEPROM_ADDR_MOTPID_I, PID_SET_MOTPID_I);
        EepromWriteValue(EEPROM_ADDR_MOTPID_D, PID_SET_MOTPID_D);
        EepromWriteValue(EEPROM_ADDR_MOTPID_MAX, PID_SET_MOTPID_MAX);
        EepromWriteValue(EEPROM_ADDR_MOTPID_MIN, PID_SET_MOTPID_MIN);

        EepromWriteValue(EEPROM_ADDR_PRESPID_P, PID_SET_PRESPID_P);
        EepromWriteValue(EEPROM_ADDR_PRESPID_I, PID_SET_PRESPID_I);
        EepromWriteValue(EEPROM_ADDR_PRESPID_D, PID_SET_PRESPID_D);
        EepromWriteValue(EEPROM_ADDR_PRESPID_MAX, PID_SET_PRESPID_MAX);
        EepromWriteValue(EEPROM_ADDR_PRESPID_MIN, PID_SET_PRESPID_MIN);

        EepromWriteValue(EEPROM_ADDR_PEEPPID_P, PID_SET_PEEPPID_P);
        EepromWriteValue(EEPROM_ADDR_PEEPPID_I, PID_SET_PEEPPID_I);
        EepromWriteValue(EEPROM_ADDR_PEEPPID_D, PID_SET_PEEPPID_D);
        EepromWriteValue(EEPROM_ADDR_PEEPPID_MAX, PID_SET_PEEPPID_MAX);
        EepromWriteValue(EEPROM_ADDR_PEEPPID_MIN, PID_SET_PEEPPID_MIN);
        PIDParamsStoreReceivedParams();
        break;
    }
    case REQUEST_FLOW_CALIBRATION_PARAMS_MSG_ID: {
        float* floats_to_receive[] = {
            &Flow_calibration_set_C0,
            &Flow_calibration_set_C1,
            &Flow_calibration_set_C2,
            &Flow_calibration_set_C3,
            &Flow_calibration_set_C4
        };
        AppReceiveFloats(_buffer, start, message_len, floats_to_receive, SIZEOF(floats_to_receive));

        //store values in nonvolatile memory
        EepromWriteValue(EEPROM_ADDR_FLOW_C0, Flow_calibration_set_C0);
        EepromWriteValue(EEPROM_ADDR_FLOW_C1, Flow_calibration_set_C1);
        EepromWriteValue(EEPROM_ADDR_FLOW_C2, Flow_calibration_set_C2);
        EepromWriteValue(EEPROM_ADDR_FLOW_C3, Flow_calibration_set_C3);
        EepromWriteValue(EEPROM_ADDR_FLOW_C4, Flow_calibration_set_C4);
        FlowParamsStoreReceivedParams();

        break;
    }
    case REQUEST_WORKPOINT_CALIBRATION_PARAMS_MSG_ID: {
        float* floats_to_receive[] = {
            &Workpoint_calibration_set_C0,
            &Workpoint_calibration_set_C1,
            &Workpoint_calibration_set_C2,
            &Workpoint_calibration_set_C3,
            &Workpoint_calibration_set_C4
        };
        AppReceiveFloats(_buffer, start, message_len, floats_to_receive, SIZEOF(floats_to_receive));

        //store values in nonvolatile memory
        EepromWriteValue(EEPROM_ADDR_WORKPOINT_C0, Workpoint_calibration_set_C0);
        EepromWriteValue(EEPROM_ADDR_WORKPOINT_C1, Workpoint_calibration_set_C1);
        EepromWriteValue(EEPROM_ADDR_WORKPOINT_C2, Workpoint_calibration_set_C2);
        EepromWriteValue(EEPROM_ADDR_WORKPOINT_C3, Workpoint_calibration_set_C3);
        EepromWriteValue(EEPROM_ADDR_WORKPOINT_C4, Workpoint_calibration_set_C4);
        WorkpointParamsStoreReceivedParams();

        break;
    }
    case REQUEST_BREATHALYSER_PARAMS_MSG_ID: {
        float* floats_to_receive[] = {
            &Breathalyser_set_Ptrig,
            &Breathalyser_set_Vtrig,
            &Breathalyser_set_Esens,
            &Breathalyser_set_IntegralSens
        };
        AppReceiveFloats(_buffer, start, message_len, floats_to_receive, SIZEOF(floats_to_receive));
        BreathalizerStoreReceivedParams();
        break;
    }
    case REQUEST_RESET_ALARM_MSG_ID: {
        Alarm_reset_code = USART2RxBufferReadByte(_buffer, start, REQUEST_RESET_ALARM_DATA_START_INDEX);
        Alarm_reset_code |= (USART2RxBufferReadByte(_buffer, start, REQUEST_RESET_ALARM_DATA_START_INDEX + 1) << 8);
        Alarm_reset_code |= (USART2RxBufferReadByte(_buffer, start, REQUEST_RESET_ALARM_DATA_START_INDEX + 2) << 16);
        Alarm_reset_code |= (USART2RxBufferReadByte(_buffer, start, REQUEST_RESET_ALARM_DATA_START_INDEX + 3) << 24);
        break;
    }
    case REQUEST_PRESSURE_SENSOR_0_CALIBRAION_MSG_ID: {
        float* floats_to_receive[] = {
            &Pressure_Sensor_0_set_calibration_C0,
            &Pressure_Sensor_0_set_calibration_C1,
            &Pressure_Sensor_0_set_calibration_C2,
            &Pressure_Sensor_0_set_calibration_C3
        };
        AppReceiveFloats(_buffer, start, message_len, floats_to_receive, SIZEOF(floats_to_receive));

        //store values in nonvolatile memory
        EepromWriteValue(EEPROM_ADDR_PRESSURE_SENSOR_0_C0, Pressure_Sensor_0_set_calibration_C0);
        EepromWriteValue(EEPROM_ADDR_PRESSURE_SENSOR_0_C1, Pressure_Sensor_0_set_calibration_C1);
        EepromWriteValue(EEPROM_ADDR_PRESSURE_SENSOR_0_C2, Pressure_Sensor_0_set_calibration_C2);
        EepromWriteValue(EEPROM_ADDR_PRESSURE_SENSOR_0_C3, Pressure_Sensor_0_set_calibration_C3);
        PressureSensor0ParamsStoreReceivedParams();

        break;
    }
    case REQUEST_PRESSURE_SENSOR_1_CALIBRAION_MSG_ID: {
        float* floats_to_receive[] = {
            &Pressure_Sensor_1_set_calibration_C0,
            &Pressure_Sensor_1_set_calibration_C1,
            &Pressure_Sensor_1_set_calibration_C2,
            &Pressure_Sensor_1_set_calibration_C3
        };
        AppReceiveFloats(_buffer, start, message_len, floats_to_receive, SIZEOF(floats_to_receive));

        //store values in nonvolatile memory
        EepromWriteValue(EEPROM_ADDR_PRESSURE_SENSOR_1_C0, Pressure_Sensor_1_set_calibration_C0);
        EepromWriteValue(EEPROM_ADDR_PRESSURE_SENSOR_1_C1, Pressure_Sensor_1_set_calibration_C1);
        EepromWriteValue(EEPROM_ADDR_PRESSURE_SENSOR_1_C2, Pressure_Sensor_1_set_calibration_C2);
        EepromWriteValue(EEPROM_ADDR_PRESSURE_SENSOR_1_C3, Pressure_Sensor_1_set_calibration_C3);
        PressureSensor1ParamsStoreReceivedParams();
        break;
    }
    case REQUEST_PRESSURE_SENSOR_2_CALIBRAION_MSG_ID: {
        float* floats_to_receive[] = {
            &Pressure_Sensor_2_set_calibration_C0,
            &Pressure_Sensor_2_set_calibration_C1,
            &Pressure_Sensor_2_set_calibration_C2,
            &Pressure_Sensor_2_set_calibration_C3
        };
        AppReceiveFloats(_buffer, start, message_len, floats_to_receive, SIZEOF(floats_to_receive));

        //store values in nonvolatile memory
        EepromWriteValue(EEPROM_ADDR_PRESSURE_SENSOR_2_C0, Pressure_Sensor_2_set_calibration_C0);
        EepromWriteValue(EEPROM_ADDR_PRESSURE_SENSOR_2_C1, Pressure_Sensor_2_set_calibration_C1);
        EepromWriteValue(EEPROM_ADDR_PRESSURE_SENSOR_2_C2, Pressure_Sensor_2_set_calibration_C2);
        EepromWriteValue(EEPROM_ADDR_PRESSURE_SENSOR_2_C3, Pressure_Sensor_2_set_calibration_C3);
        PressureSensor2ParamsStoreReceivedParams();
        break;
    }
    case REQUEST_PRESSURE_SENSOR_3_CALIBRAION_MSG_ID: {
        float* floats_to_receive[] = {
            &Pressure_Sensor_3_set_calibration_C0,
            &Pressure_Sensor_3_set_calibration_C1,
            &Pressure_Sensor_3_set_calibration_C2,
            &Pressure_Sensor_3_set_calibration_C3
        };
        AppReceiveFloats(_buffer, start, message_len, floats_to_receive, SIZEOF(floats_to_receive));

        //store values in nonvolatile memory
        EepromWriteValue(EEPROM_ADDR_PRESSURE_SENSOR_3_C0, Pressure_Sensor_3_set_calibration_C0);
        EepromWriteValue(EEPROM_ADDR_PRESSURE_SENSOR_3_C1, Pressure_Sensor_3_set_calibration_C1);
        EepromWriteValue(EEPROM_ADDR_PRESSURE_SENSOR_3_C2, Pressure_Sensor_3_set_calibration_C2);
        EepromWriteValue(EEPROM_ADDR_PRESSURE_SENSOR_3_C3, Pressure_Sensor_3_set_calibration_C3);
        PressureSensor3ParamsStoreReceivedParams();
        break;
    }
    default:
        // unknown message, discard that data
        break;
    }
}

void AppCheckForNewIncomingMessage(void)
{
    incoming_buffer_head = USART2GetCurrentBufferHead();
    switch (incoming_message_process_step) {
    case waiting_for_start_mark:
        if (USART2RxBufferGetBytesInBuffer(incoming_buffer_head, incoming_buffer_tail) >= 7) {
            // wait for at least 4 bytes
            // check if we have start markers
            if (USART2RxBufferReadByte(USART2_Rx_Buffer, incoming_buffer_tail, 0) == 0xF0 && USART2RxBufferReadByte(USART2_Rx_Buffer, incoming_buffer_tail, 1) == 0x0F) {
                // yes, we do!
                // let's check message ID and declared length
                current_message.message_id = USART2RxBufferReadByte(USART2_Rx_Buffer, incoming_buffer_tail, 2);
                current_message.message_length = USART2RxBufferReadByte(USART2_Rx_Buffer, incoming_buffer_tail, 3);

                // length should be greater than zero and id should be in supported
                // range
                if (current_message.message_length > 0 && current_message.message_id > 0) {
                    incoming_message_process_step = waiting_for_rest_of_message;
                } else {
                    //error, message length = 0 od id = 0, fake positive message start mark
                    if (incoming_buffer_tail >= (USART2_RX_BUFFER_SIZE - 1)) {
                        incoming_buffer_tail = 0;
                    } else {
                        incoming_buffer_tail++;
                    }
                }
            } else {
                //no start mark here, move to next byte!
                if (incoming_buffer_tail >= (USART2_RX_BUFFER_SIZE - 1)) {
                    incoming_buffer_tail = 0;
                } else {
                    incoming_buffer_tail++;
                }
            }
        } else {
            //no new data (or not enough data) nothing to do here!
            return;
        }
        break;
    case waiting_for_rest_of_message:
        // wait for the rest of message
        if (USART2RxBufferGetBytesInBuffer(incoming_buffer_head, incoming_buffer_tail) >= current_message.message_length) {
            // bytes arrived, check if end mark is where it's supposed to be
            if (USART2RxBufferReadByte(USART2_Rx_Buffer, incoming_buffer_tail, (current_message.message_length - 1)) == 0xAA) {
                // yep, end mark present, process message
                uint16_t calc_checksum = checksum((USART2_Rx_Buffer + incoming_buffer_tail + MESSAGE_HEADER_LEN),
                    current_message.message_length - MESSAGE_HEADER_LEN - MESSAGE_FOOTER_LEN,
                    iso_3309);
                uint8_t crc_lo = calc_checksum & 0xFF;
                uint8_t crc_hi = (calc_checksum >> 8) & 0xFF;
                // checksum check
                if ((USART2RxBufferReadByte(USART2_Rx_Buffer, incoming_buffer_tail, (current_message.message_length - 3)) == crc_lo) && (USART2RxBufferReadByte(USART2_Rx_Buffer, incoming_buffer_tail, (current_message.message_length - 2)) == crc_hi)) {
                    // if (1) {
                    AppProcessReceivedMessage(USART2_Rx_Buffer, incoming_buffer_tail, current_message.message_length);
                    incoming_buffer_tail += current_message.message_length;
                    incoming_message_process_step = waiting_for_start_mark;
                } else {
                    //checksum check failed
                    if (incoming_buffer_tail >= (USART2_RX_BUFFER_SIZE - 1)) {
                        incoming_buffer_tail = 0;
                    } else {
                        incoming_buffer_tail++;
                    }
                    incoming_message_process_step = waiting_for_start_mark;
                }
            } else {
                //no end mark - discard that message and process next bytes
                if (incoming_buffer_tail >= (USART2_RX_BUFFER_SIZE - 1)) {
                    incoming_buffer_tail = 0;
                } else {
                    incoming_buffer_tail++;
                }
                incoming_message_process_step = waiting_for_start_mark;
            }
        } else {
            //not enough bytes
            break;
        }
        break;
    case processing_message:
        break;
    default:
        incoming_message_process_step = waiting_for_start_mark;
        break;
    }
}

void AppSendMainParams(void)
{
    float* floats_to_send[] = {
        &ventilator_current_pressure,
        &ventilator_current_flow,
        &ventilator_current_inhPresMean,
        &ventilator_current_exhPresMean,
        &ventilator_current_inhVol,
        &ventilator_current_inhVolPerMin,
        &ventilator_current_ExhVol,
        &ventilator_current_BreathPerMin,
        &ventilator_current_LungCompl,
        &ventilator_current_RotTarget
    };
    uint8_t* uints8_to_send[] = {
        &ventilator_current_LstInhTyp,
        &ventilator_current_BreathPhase
    };

    AppFillInOutputBufferAndSend(MAIN_PARAMS_MSG_ID, MAIN_PARAMS_MSG_LEN, NULL, 0, floats_to_send, SIZEOF(floats_to_send), uints8_to_send, SIZEOF(uints8_to_send));
}

void AppSendMedicalSettingsParams(void)
{
    float* floats_to_send[] = {
        &ventilator_medical_PEEP,
        &ventilator_medical_TargetFiO2,
        &ventilator_medical_PIP,
        &ventilator_medical_IEratio,
        &ventilator_medical_BreathPerMin,
        &ventilator_medical_TargetVol
    };
    uint8_t* uints8_to_send[] = {
        &ventilator_medical_Mode
    };

    AppFillInOutputBufferAndSend(MEDICAL_SETTINGS_PARAMS_MSG_ID, MEDICAL_SETTINGS_PARAMS_MSG_LEN, NULL, 0, floats_to_send, SIZEOF(floats_to_send), uints8_to_send, SIZEOF(uints8_to_send));
}

void AppSendPIDSettingsParams(void)
{
    float* floats_to_send[] = {
        &PID_MOTPID_P,
        &PID_MOTPID_I,
        &PID_MOTPID_D,
        &PID_MOTPID_MAX,
        &PID_MOTPID_MIN,
        &PID_PRESPID_P,
        &PID_PRESPID_I,
        &PID_PRESPID_D,
        &PID_PRESPID_MAX,
        &PID_PRESPID_MIN,
        &PID_PEEPPID_P,
        &PID_PEEPPID_I,
        &PID_PEEPPID_D,
        &PID_PEEPPID_MAX,
        &PID_PEEPPID_MIN
    };

    AppFillInOutputBufferAndSend(PID_SETTINGS_PARAMS_MSG_ID, PID_SETTINGS_PARAMS_MSG_LEN, NULL, 0, floats_to_send, SIZEOF(floats_to_send), NULL, 0);
}

void AppSendFlowCalibrationParams(void)
{
    float* floats_to_send[] = {
        &Flow_calibration_C0,
        &Flow_calibration_C1,
        &Flow_calibration_C2,
        &Flow_calibration_C3,
        &Flow_calibration_C4
    };

    AppFillInOutputBufferAndSend(FLOW_CALIBRATION_PARAMS_MSG_ID, FLOW_CALIBRATION_PARAMS_MSG_LEN, NULL, 0, floats_to_send, SIZEOF(floats_to_send), NULL, 0);
}

void AppSendWorkpointCalibrationParams(void)
{
    float* floats_to_send[] = {
        &Workpoint_calibration_C0,
        &Workpoint_calibration_C1,
        &Workpoint_calibration_C2,
        &Workpoint_calibration_C3,
        &Workpoint_calibration_C4
    };

    AppFillInOutputBufferAndSend(WORKPOINT_CALIBRATION_PARAMS_MSG_ID, WORKPOINT_CALIBRATION_PARAMS_MSG_LEN, NULL, 0, floats_to_send, SIZEOF(floats_to_send), NULL, 0);
}

void AppSendBreathalyserParams(void)
{
    float* floats_to_send[] = {
        &Breathalyser_Ptrig,
        &Breathalyser_Vtrig,
        &Breathalyser_Esens,
        &Breathalyser_IntegralSens
    };

    AppFillInOutputBufferAndSend(BREATHALYSER_PARAMS_MSG_ID, BREATHALYSER_PARAMS_MSG_LEN, NULL, 0, floats_to_send, SIZEOF(floats_to_send), NULL, 0);
}

void AppSendAlarmStatusMessage(void)
{
    uint32_t* uints32_to_send[] = {
        &Alarm_code
    };

    AppFillInOutputBufferAndSend(ALARM_STATUS_MSG_ID, ALARM_STATUS_MSG_LEN, uints32_to_send, SIZEOF(uints32_to_send), NULL, 0, NULL, 0);
}

void AppSendAlarmDetailsMessage(void)
{
    uint32_t* uints32_to_send[] = {
        &Alarm_code_detail,
        &Alarm_detail
    };

    AppFillInOutputBufferAndSend(ALARM_DETAILS_MSG_ID, ALARM_DETAILS_MSG_LEN, uints32_to_send, SIZEOF(uints32_to_send), NULL, 0, NULL, 0);
}

void AppSendPressureSensor_0_CalibrationMessage(void)
{
    float* floats_to_send[] = {
        &Pressure_Sensor_0_calibration_C0,
        &Pressure_Sensor_0_calibration_C1,
        &Pressure_Sensor_0_calibration_C2,
        &Pressure_Sensor_0_calibration_C3
    };

    AppFillInOutputBufferAndSend(PRESSURE_SENSOR_0_CALIBRAION_MSG_ID, PRESSURE_SENSOR_0_CALIBRAION_MSG_LEN, NULL, 0, floats_to_send, SIZEOF(floats_to_send), NULL, 0);
}

void AppSendPressureSensor_1_CalibrationMessage(void)
{
    float* floats_to_send[] = {
        &Pressure_Sensor_1_calibration_C0,
        &Pressure_Sensor_1_calibration_C1,
        &Pressure_Sensor_1_calibration_C2,
        &Pressure_Sensor_1_calibration_C3
    };

    AppFillInOutputBufferAndSend(PRESSURE_SENSOR_1_CALIBRAION_MSG_ID, PRESSURE_SENSOR_1_CALIBRAION_MSG_LEN, NULL, 0, floats_to_send, SIZEOF(floats_to_send), NULL, 0);
}

void AppSendPressureSensor_2_CalibrationMessage(void)
{
    float* floats_to_send[] = {
        &Pressure_Sensor_2_calibration_C0,
        &Pressure_Sensor_2_calibration_C1,
        &Pressure_Sensor_2_calibration_C2,
        &Pressure_Sensor_2_calibration_C3
    };

    AppFillInOutputBufferAndSend(PRESSURE_SENSOR_2_CALIBRAION_MSG_ID, PRESSURE_SENSOR_2_CALIBRAION_MSG_LEN, NULL, 0, floats_to_send, SIZEOF(floats_to_send), NULL, 0);
}

void AppSendPressureSensor_3_CalibrationMessage(void)
{
    float* floats_to_send[] = {
        &Pressure_Sensor_3_calibration_C0,
        &Pressure_Sensor_3_calibration_C1,
        &Pressure_Sensor_3_calibration_C2,
        &Pressure_Sensor_3_calibration_C3
    };

    AppFillInOutputBufferAndSend(PRESSURE_SENSOR_3_CALIBRAION_MSG_ID, PRESSURE_SENSOR_3_CALIBRAION_MSG_LEN, NULL, 0, floats_to_send, SIZEOF(floats_to_send), NULL, 0);
}

void AppSendDebugParams(void)
{
    float* floats_to_send[] = {
        &Pressure_Sensor_0,
        &Pressure_Sensor_1,
        &Pressure_Sensor_2,
        &Pressure_Sensor_3,
        &MotorOut,
        &MotorRPM,
        &MotorCurr,
        &MotorTemp,
        &ventilator_DeviceTemp,
        &ventilator_AmbientTemp,
        &ventilator_AmbientHumidity,
        &ventilator_PEEPOut
    };

    AppFillInOutputBufferAndSend(DEBUG_PARAMS_MSG_ID, DEBUG_PARAMS_MSG_LEN, NULL, 0, floats_to_send, SIZEOF(floats_to_send), NULL, 0);
}
