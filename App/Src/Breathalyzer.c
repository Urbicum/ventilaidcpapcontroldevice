/*
 * Breathalyzer.c
 *
 *  Created on: May 18, 2020
 *      Author: ventilaid
 */
#include "Breathalyzer.h"
#include <PidController.h>

#include "LED.h"

extern float Flow;
float CalFlowSum = 0;
float CalFlowAvg = 0;
extern float FlowEps;
float Error = 0;
extern float PressPAvg;
extern float PressFAvg;

float PeakFlow = 0;
float LvlExh = 2.3;
float LvlInhP = 2;
float LvlInhF = 15;
float PrVPid = 0;
uint8_t PhaseMinDuration = 100;
float ESens = 0.25;
float TrigFlow = 10;
float LeakTrigger = 0;
float FlowPeak = 0;
float FlowIntegralInhale = 0;
float FlowMax = 1000;
float uFlow = 0;
uint8_t State = dOther;
uint8_t NextState = dOther;

uint16_t LowHolder = 0;
uint16_t HighHolder = 0;
uint16_t ExhCountTrh = 0;
uint16_t InhCountTrh = 4;
uint8_t InhaleValue = 0;

extern PIDControl* pid;

typedef struct {
    float Flow;
    float PrVPid;
    uint8_t isInh;
    uint8_t isExh;
    uint8_t isBlock;
    uint8_t isLeak;
} CalSample;

Breath BreathBuff[16];
uint8_t BreathBuffIndex = 0;
uint8_t BreathBuffReady = 0;
uint8_t BreathBuffMaxSample = 8;

// float PidBuff[32];
// uint8_t PidBuffIndex=0;
// uint8_t PidBuffReady=0;
// uint8_t PidMaxSample=20;
// float PidSum=0;
// float PidAvg=0;

CalSample CalBuff[128];
uint16_t CalBuffIndex = 0;
uint8_t CalBuffReady = 0;
const uint16_t CalMaxSample = 10;
uint16_t CalExhCount = 0;
uint16_t CalInhCount = 0;

//Stwarzanie nowego oddechu,
//dopisywanie nowego odddechy do tabelki
//Pierwszy nowy stworzyć w inicie.
//Kolejny oddech stwarzać, gdy wykryje wdech.
//Update Breath

void CreateBreath()
{
    uint8_t PreviousBreath = BreathBuffIndex;

    BreathBuffIndex++;
    if (BreathBuffMaxSample == BreathBuffIndex) {
        BreathBuffReady = 1;
        BreathBuffIndex = 0;
    }

    BreathBuff[BreathBuffIndex].TiInhaleStart = HAL_GetTick();
    BreathBuff[BreathBuffIndex].TiPreviousInhlae = BreathBuff[PreviousBreath].TiInhaleStart;
    BreathBuff[BreathBuffIndex].State = dInhale;

    BreathBuff[BreathBuffIndex].TiInhaleEnd = 0;
    BreathBuff[BreathBuffIndex].TiExhStart = 0;
    BreathBuff[BreathBuffIndex].TiExhEnd = 0;
    BreathBuff[BreathBuffIndex].TiExhForce = 0;
    BreathBuff[BreathBuffIndex].FlowIntegralExhale = 0;
    BreathBuff[BreathBuffIndex].FlowIntegralInhale = 0;
    BreathBuff[BreathBuffIndex].FlowPeakExhale = 0;
    BreathBuff[BreathBuffIndex].FlowPeakInhale = 0;
    BreathBuff[BreathBuffIndex].PressMinInhale = 0;
    BreathBuff[BreathBuffIndex].PressMinExhale = 0;
    BreathBuff[BreathBuffIndex].PressMaxExhale = 0;
    BreathBuff[BreathBuffIndex].PressMaxInhale = 0;
    BreathBuff[BreathBuffIndex].PressMeaExhale = 0;
    BreathBuff[BreathBuffIndex].PressMeaInhale = 0;
    BreathBuff[BreathBuffIndex].iii = 0;
    BreathBuff[BreathBuffIndex].TrigExEn = 0;
    BreathBuff[BreathBuffIndex].TrigExSt = 0;
    BreathBuff[BreathBuffIndex].TrigInEn = 0;
    BreathBuff[BreathBuffIndex].TrigInSt = 0;
    BreathBuff[BreathBuffIndex].ForceExhale = 0;
    BreathBuff[BreathBuffIndex].GotExhale = 0;
    BreathBuff[BreathBuffIndex].ExhaleWaitingTime = 0;
    //    BreathBuff[BreathBuffIndex].State = dOther;
}

void Breathalizer()
{
    uFlow = Flow;
    FlowIntegralInhale = BreathBuff[BreathBuffIndex].FlowIntegralInhale;
    FlowPeak = BreathBuff[BreathBuffIndex].FlowPeakInhale;
    State = BreathBuff[BreathBuffIndex].State;

    if (uFlow > TrigFlow) {
        NextState = dInhale;
    } else if ((0 < uFlow) && (uFlow < ESens * FlowPeak) && ((dInhale == State) || (dPostInhale == State))) {
        NextState = dPostInhale;
    } else if ((uFlow > FlowEps) && (dInhale == State)) {
        NextState = dInhale;
    } else if ((uFlow < -FlowEps)) {
        NextState = dExhale;
    } else if ((uFlow < 0) && ((dExhale == State) || (dPostExhale == State))) {
        NextState = dPostExhale;
    } else {
        NextState = dOther;
    }

    if (((dPostExhale <= State)) && (dInhale == NextState)) {
        CreateBreath();
    } else {
        if (dExhale == NextState) {
            BreathBuff[BreathBuffIndex].GotExhale = 1;
            BreathBuff[BreathBuffIndex].TiExhStart = HAL_GetTick();
        } else if ((FlowIntegralInhale > FlowMax) || (dPostInhale == NextState)) {
            BreathBuff[BreathBuffIndex].ForceExhale = 1;
            BreathBuff[BreathBuffIndex].TiExhForce = HAL_GetTick();
        }
    }

    if ((1 == BreathBuff[BreathBuffIndex].ForceExhale) && (0 == BreathBuff[BreathBuffIndex].GotExhale)) {
        BreathBuff[BreathBuffIndex].ExhaleWaitingTime++;
    }
    if (BreathBuff[BreathBuffIndex].ExhaleWaitingTime > LeakTrigger)
        NextState = dLeak;

    BreathBuff[BreathBuffIndex].State = NextState;

    if ((dInhale == NextState) || (dPostInhale == NextState)) {
        BreathBuff[BreathBuffIndex].FlowIntegralInhale += Flow / (float)dSamplesPerMinute;
        if (Flow > BreathBuff[BreathBuffIndex].FlowPeakInhale) {
            BreathBuff[BreathBuffIndex].FlowPeakInhale = Flow;
        }
    }

    if ((dExhale == NextState) || (dPostExhale == NextState)) {
        BreathBuff[BreathBuffIndex].FlowIntegralExhale += Flow / (float)dSamplesPerMinute;
        if (Flow > BreathBuff[BreathBuffIndex].FlowPeakExhale) {
            BreathBuff[BreathBuffIndex].FlowPeakExhale = Flow;
        }
    }
}

void CalUpdate()
{
    if (CalBuffReady == 1) {
        CalExhCount -= CalBuff[CalBuffIndex].isExh;
        CalInhCount -= CalBuff[CalBuffIndex].isInh;
        CalFlowSum -= CalBuff[CalBuffIndex].Flow;
    }
    CalBuff[CalBuffIndex].Flow = Flow;
    CalBuff[CalBuffIndex].PrVPid = (1000 * PressFAvg / pid->output) - pid->setpoint;
    PrVPid = CalBuff[CalBuffIndex].PrVPid;
    Error = pid->setpoint - PressFAvg;

    CalFlowSum += CalBuff[CalBuffIndex].Flow;

    if (0 == CalBuffReady) {
        CalFlowAvg = CalFlowSum / CalBuffIndex;
    } else {
        CalFlowAvg = CalFlowSum / CalMaxSample;
    }

    // Inhale!
    CalBuff[CalBuffIndex].isInh = 0;

    if (Error > LvlInhP) {
        //	digitalWrite(ledPinGre, LOW);
        CalBuff[CalBuffIndex].isInh++;
    }
    if (Flow > LvlInhF) {
        //	digitalWrite(ledPinGre, LOW);
        CalBuff[CalBuffIndex].isInh++;
    }
    if (2 == CalBuff[CalBuffIndex].isInh) {
        //LEDSetRGB(0, 0, 255);
    } else if (1 == CalBuff[CalBuffIndex].isInh) {
        //LEDSetRGB(0, 255, 255);
    } else {
        //LEDSetRGB(255, 0, 0);
    }
    InhaleValue = CalBuff[CalBuffIndex].isInh;

    // Exhale!
    if (CalBuff[CalBuffIndex].PrVPid > LvlExh) {
        //	digitalWrite(ledPinGre, LOW);
        CalBuff[CalBuffIndex].isExh = 1;
    } else {
        //	digitalWrite(ledPinGre, HIGH);
        CalBuff[CalBuffIndex].isExh = 0;
    }

    CalExhCount += CalBuff[CalBuffIndex].isExh;
    CalInhCount += CalBuff[CalBuffIndex].isInh;

    CalBuffIndex++;
    if (CalMaxSample == CalBuffIndex) {
        CalBuffReady = 1;
        CalBuffIndex = 0;
    }
}

void BreathalizerStoreReceivedParams(void)
{
    Breathalyser_Ptrig = Breathalyser_set_Ptrig;
    Breathalyser_Vtrig = Breathalyser_set_Vtrig;
    Breathalyser_Esens = Breathalyser_set_Esens;
    Breathalyser_IntegralSens = Breathalyser_set_IntegralSens;
}
