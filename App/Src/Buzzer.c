/*
 * Peep.c
 *
 *  Created on: 06.05.2020
 *      Author: Maciej
 */

#include "Buzzer.h"

void BuzzerInit(void)
{
    HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, 0);
}

void BuzzerSetOutput(uint8_t _output)
{
    HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, _output);
}
