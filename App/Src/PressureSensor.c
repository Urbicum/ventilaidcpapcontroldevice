/*
 * PressureSensor.c
 *
 *  Created on: 01.04.2020
 *      Author: Maciej
 */

#include "PressureSensor.h"

#include "Eeprom.h"
#include "EepromMemoryMap.h"
#include "Error.h"
#include "spi.h"

typedef struct {
    float Press[4];
} PressSample;

PressSample PressBuff[256];
const uint16_t PressMaxSample = 20;
uint16_t PressBuffIndex = 0;
uint8_t PressBuffReady = 0;
uint32_t PressTmp[4];
uint32_t PressSum[4];
uint32_t PressAvgInt[4];
float PressAvg[4];
float PressFAvgRaw = 0;
float PressPAvg = 0;
float PressFAvg = 0;

void PressAverage()
{
    for (uint8_t iii = 0; iii < NUMBER_OF_PRESSURE_SENSORS; iii++) {
        if (PressBuffReady == 1) {
            PressAvgInt[iii] = PressSum[iii] / PressMaxSample;
        } else {
            PressAvgInt[iii] = PressSum[iii] / (PressBuffIndex);
        }
        PressAvg[iii] = (PsiTocm * ((((float)PressAvgInt[iii] / (float)PressIntMulti) - PressureParameters[iii].OutMin) * PressureParameters[iii].Multip + PressureParameters[iii].PreMin));
    }
    PressPAvg = PressAvg[3];
    PressFAvg = PressAvg[2];
    PressFAvgRaw = ((float)PressAvgInt[2] / (float)PressIntMulti);
}

void PressureUpdate(void)
{
    for (uint8_t iii = 0; iii < NUMBER_OF_PRESSURE_SENSORS; iii++) {
        PressTmp[iii] = PressIntMulti * (uint32_t)pressure_sensor_reading[iii];
        if (PressBuffReady == 1) {
            PressSum[iii] -= PressBuff[PressBuffIndex].Press[iii];
        }
        PressSum[iii] += PressTmp[iii];
        PressBuff[PressBuffIndex].Press[iii] = PressTmp[iii];
    }
    PressBuffIndex++;

    if (PressMaxSample == PressBuffIndex) {
        PressBuffReady = 1;
        PressBuffIndex = 0;
    }
}

void PressureSensorInit(void)
{
    PressureParameters[0].PreMax = 1;
    PressureParameters[0].PreMin = 0;
    PressureParameters[0].OutMax = 14746;
    PressureParameters[0].OutMin = 1638;
    PressureParameters[0].Multip = (PressureParameters[0].PreMax - PressureParameters[0].PreMin) / (PressureParameters[0].OutMax - PressureParameters[0].OutMin);

    PressureParameters[1].PreMax = 0.0723;
    PressureParameters[1].PreMin = -0.0723;
    PressureParameters[1].OutMax = 14746;
    PressureParameters[1].OutMin = 1638;
    PressureParameters[1].Multip = (PressureParameters[1].PreMax - PressureParameters[1].PreMin) / (PressureParameters[1].OutMax - PressureParameters[1].OutMin);

    PressureParameters[2].PreMax = 0.0723;
    PressureParameters[2].PreMin = -0.0723;
    PressureParameters[2].OutMax = 14746;
    PressureParameters[2].OutMin = 1638;
    PressureParameters[2].Multip = (PressureParameters[2].PreMax - PressureParameters[2].PreMin) / (PressureParameters[2].OutMax - PressureParameters[2].OutMin);

    PressureParameters[3].PreMax = 1;
    PressureParameters[3].PreMin = 0;
    PressureParameters[3].OutMax = 14746;
    PressureParameters[3].OutMin = 1638;
    PressureParameters[3].Multip = (PressureParameters[3].PreMax - PressureParameters[3].PreMin) / (PressureParameters[3].OutMax - PressureParameters[3].OutMin);

    for (uint8_t iii = 0; iii < NUMBER_OF_PRESSURE_SENSORS; iii++) {
        pressure_sensor_reading[iii] = 0;
        PressSum[iii] = 0;
        PressTmp[iii] = 0;
        PressAvg[iii] = 0;
    }
    HAL_GPIO_WritePin(SPI1_CS1_GPIO_Port, SPI1_CS1_Pin, 1);
    HAL_GPIO_WritePin(SPI1_CS2_GPIO_Port, SPI1_CS2_Pin, 1);
    HAL_GPIO_WritePin(SPI1_CS3_GPIO_Port, SPI1_CS3_Pin, 1);
    HAL_GPIO_WritePin(SPI1_CS4_GPIO_Port, SPI1_CS4_Pin, 1);
}

void PressureSensorUpdateMeasurements(void)
{
    for (uint8_t iii = 0; iii < NUMBER_OF_PRESSURE_SENSORS; iii++) {
        switch (iii) {
        case 0:
            HAL_GPIO_WritePin(SPI1_CS1_GPIO_Port, SPI1_CS1_Pin, 0);
            break;
        case 1:
            HAL_GPIO_WritePin(SPI1_CS2_GPIO_Port, SPI1_CS2_Pin, 0);
            break;
        case 2:
            HAL_GPIO_WritePin(SPI1_CS3_GPIO_Port, SPI1_CS3_Pin, 0);
            break;
        case 3:
            HAL_GPIO_WritePin(SPI1_CS4_GPIO_Port, SPI1_CS4_Pin, 0);
            break;
        }

        uint8_t values[2];
        values[0] = SPI1_Send_Receive_Byte(0x1a);
        values[1] = SPI1_Send_Receive_Byte(0x1a);

        switch (iii) {
        case 0:
            HAL_GPIO_WritePin(SPI1_CS1_GPIO_Port, SPI1_CS1_Pin, 1);
            break;
        case 1:
            HAL_GPIO_WritePin(SPI1_CS2_GPIO_Port, SPI1_CS2_Pin, 1);
            break;
        case 2:
            HAL_GPIO_WritePin(SPI1_CS3_GPIO_Port, SPI1_CS3_Pin, 1);
            break;
        case 3:
            HAL_GPIO_WritePin(SPI1_CS4_GPIO_Port, SPI1_CS4_Pin, 1);
            break;
        }

        pressure_sensor_status[iii] = ((values[0] & 0xC0) >> 6);
        pressure_sensor_reading[iii] = ((values[0] & 0x3F) << 8) | (values[1]);

        if (pressure_sensor_status != 0) {
            //TODO: should'n there be [iii] in line above? you want to check value, right?
            //error in reading sensor status
            ErrorSendMessage(3);
        }
    }
    PressureUpdate();
}

void PressureSensorParamsLoadFromEeprom(void)
{

    EepromReadValue(EEPROM_ADDR_PRESSURE_SENSOR_0_C0, &Pressure_Sensor_0_calibration_C0);
    EepromReadValue(EEPROM_ADDR_PRESSURE_SENSOR_0_C1, &Pressure_Sensor_0_calibration_C1);
    EepromReadValue(EEPROM_ADDR_PRESSURE_SENSOR_0_C2, &Pressure_Sensor_0_calibration_C2);
    EepromReadValue(EEPROM_ADDR_PRESSURE_SENSOR_0_C3, &Pressure_Sensor_0_calibration_C3);

    EepromReadValue(EEPROM_ADDR_PRESSURE_SENSOR_1_C0, &Pressure_Sensor_1_calibration_C0);
    EepromReadValue(EEPROM_ADDR_PRESSURE_SENSOR_1_C1, &Pressure_Sensor_1_calibration_C1);
    EepromReadValue(EEPROM_ADDR_PRESSURE_SENSOR_1_C2, &Pressure_Sensor_1_calibration_C2);
    EepromReadValue(EEPROM_ADDR_PRESSURE_SENSOR_1_C3, &Pressure_Sensor_1_calibration_C3);

    EepromReadValue(EEPROM_ADDR_PRESSURE_SENSOR_2_C0, &Pressure_Sensor_2_calibration_C0);
    EepromReadValue(EEPROM_ADDR_PRESSURE_SENSOR_2_C1, &Pressure_Sensor_2_calibration_C1);
    EepromReadValue(EEPROM_ADDR_PRESSURE_SENSOR_2_C2, &Pressure_Sensor_2_calibration_C2);
    EepromReadValue(EEPROM_ADDR_PRESSURE_SENSOR_2_C3, &Pressure_Sensor_2_calibration_C3);

    EepromReadValue(EEPROM_ADDR_PRESSURE_SENSOR_3_C0, &Pressure_Sensor_3_calibration_C0);
    EepromReadValue(EEPROM_ADDR_PRESSURE_SENSOR_3_C1, &Pressure_Sensor_3_calibration_C1);
    EepromReadValue(EEPROM_ADDR_PRESSURE_SENSOR_3_C2, &Pressure_Sensor_3_calibration_C2);
    EepromReadValue(EEPROM_ADDR_PRESSURE_SENSOR_3_C3, &Pressure_Sensor_3_calibration_C3);
}

void PressureSensor0ParamsStoreReceivedParams(void)
{
    Pressure_Sensor_0_calibration_C0 = Pressure_Sensor_0_set_calibration_C0;
    Pressure_Sensor_0_calibration_C1 = Pressure_Sensor_0_set_calibration_C1;
    Pressure_Sensor_0_calibration_C2 = Pressure_Sensor_0_set_calibration_C2;
    Pressure_Sensor_0_calibration_C3 = Pressure_Sensor_0_set_calibration_C3;
}

void PressureSensor1ParamsStoreReceivedParams(void)
{
    Pressure_Sensor_1_calibration_C0 = Pressure_Sensor_1_set_calibration_C0;
    Pressure_Sensor_1_calibration_C1 = Pressure_Sensor_1_set_calibration_C1;
    Pressure_Sensor_1_calibration_C2 = Pressure_Sensor_1_set_calibration_C2;
    Pressure_Sensor_1_calibration_C3 = Pressure_Sensor_1_set_calibration_C3;
}

void PressureSensor2ParamsStoreReceivedParams(void)
{
    Pressure_Sensor_2_calibration_C0 = Pressure_Sensor_2_set_calibration_C0;
    Pressure_Sensor_2_calibration_C1 = Pressure_Sensor_2_set_calibration_C1;
    Pressure_Sensor_2_calibration_C2 = Pressure_Sensor_2_set_calibration_C2;
    Pressure_Sensor_2_calibration_C3 = Pressure_Sensor_2_set_calibration_C3;
}

void PressureSensor3ParamsStoreReceivedParams(void)
{
    Pressure_Sensor_3_calibration_C0 = Pressure_Sensor_3_set_calibration_C0;
    Pressure_Sensor_3_calibration_C1 = Pressure_Sensor_3_set_calibration_C1;
    Pressure_Sensor_3_calibration_C2 = Pressure_Sensor_3_set_calibration_C2;
    Pressure_Sensor_3_calibration_C3 = Pressure_Sensor_3_set_calibration_C3;
}
