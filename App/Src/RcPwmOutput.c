/*
 * MotorOutput.c
 *
 *  Created on: 01.04.2020
 *      Author: Maciej
 */

#include "tim.h"
#include <RcPwmOutput.h>

void RcPwmOutputInit(void)
{
    HAL_TIM_PWM_Start(&htim1, MOTOR_OUTPUT_CHANNEL);
    HAL_TIM_PWM_Start(&htim1, SERVO0_OUTPUT_CHANNEL);
    HAL_TIM_PWM_Start(&htim1, SERVO1_OUTPUT_CHANNEL);
    __HAL_TIM_SET_COMPARE(&htim1, MOTOR_OUTPUT_CHANNEL, RCPWM_AT_ZERO);
    __HAL_TIM_SET_COMPARE(&htim1, SERVO0_OUTPUT_CHANNEL, RCPWM_SERVO_AT_ZERO);
    __HAL_TIM_SET_COMPARE(&htim1, SERVO1_OUTPUT_CHANNEL, RCPWM_SERVO_AT_ZERO);
}

void RcPwmMotorSetOutput(uint16_t _output_level)
{
    if (_output_level > RCPWM_MAP_MAX) {
        _output_level = RCPWM_MAP_MAX;
    }
    if (_output_level == RCPWM_MAP_MIN) {
        __HAL_TIM_SET_COMPARE(&htim1, MOTOR_OUTPUT_CHANNEL, RCPWM_AT_ZERO);
    }
    // set rcpwm output
    uint16_t output_value;
    output_value = (uint16_t)((((float)RCPWM_OUT_MAX - (float)RCPWM_OUT_MIN) / (RCPWM_MAP_MAX * 1.0f)) * (float)_output_level) + RCPWM_OUT_MIN;
    __HAL_TIM_SET_COMPARE(&htim1, MOTOR_OUTPUT_CHANNEL, output_value);
}

void RcPwmServo0SetOutput(uint16_t _output_level)
{
    if (_output_level > RCPWM_MAP_MAX) {
        _output_level = RCPWM_MAP_MAX;
    }
    if (_output_level == RCPWM_MAP_MIN) {
        __HAL_TIM_SET_COMPARE(&htim1, SERVO0_OUTPUT_CHANNEL, RCPWM_SERVO_AT_ZERO);
    }
    // set rcpwm output
    uint16_t output_value;
    output_value = (uint16_t)((((float)RCPWM_SERVO_OUT_MAX - (float)RCPWM_SERVO_OUT_MIN) / (RCPWM_MAP_MAX * 1.0f)) * (float)_output_level) + RCPWM_SERVO_OUT_MIN;
    __HAL_TIM_SET_COMPARE(&htim1, SERVO0_OUTPUT_CHANNEL, output_value);
}

void RcPwmServo1SetOutput(uint16_t _output_level)
{
    if (_output_level > RCPWM_MAP_MAX) {
        _output_level = RCPWM_MAP_MAX;
    }
    if (_output_level == RCPWM_MAP_MIN) {
        __HAL_TIM_SET_COMPARE(&htim1, SERVO1_OUTPUT_CHANNEL, RCPWM_SERVO_AT_ZERO);
    }
    // set rcpwm output
    uint16_t output_value;
    output_value = (uint16_t)((((float)RCPWM_SERVO_OUT_MAX - (float)RCPWM_SERVO_OUT_MIN) / (RCPWM_MAP_MAX * 1.0f)) * (float)_output_level) + RCPWM_SERVO_OUT_MIN;
    __HAL_TIM_SET_COMPARE(&htim1, SERVO1_OUTPUT_CHANNEL, output_value);
}
void RcPwmServo0SetOutputRaw(uint16_t _output_level)
{
    __HAL_TIM_SET_COMPARE(&htim1, SERVO0_OUTPUT_CHANNEL, _output_level);
}
void RcPwmServo1SetOutputRaw(uint16_t _output_level)
{
    __HAL_TIM_SET_COMPARE(&htim1, SERVO1_OUTPUT_CHANNEL, _output_level);
}
